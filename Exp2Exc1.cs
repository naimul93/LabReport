﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReport
{
    public class Exp2Exc1
    {
        public static void Exc1()
        {
            int x, y;
            Console.WriteLine("Enter the value of X in XY coordinate:   ");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the value Y in XY coordinate:  ");
            y = int.Parse(Console.ReadLine());
            if(x==0 && y==0)
            {
                Console.WriteLine("The point is in 0,0 position");
            }
            else if(x>0 && y>0)
            {
                Console.WriteLine("The point is in  above right position");
            }
            else if (x > 0 && y < 0)
            {
                Console.WriteLine("The point is in below right position");
            }
            else if (x < 0 && y > 0)
            {
                Console.WriteLine("The point is in above left position");
            }
            else 
            {
                Console.WriteLine("The point is in  below left position");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReport
{
    public class Experiment1
    {
        public static void Exp1()
        {
            Console.WriteLine("Enter first number:  ");
            int num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter 2nd number:  ");
            int num2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter 3rd number:  ");
            int num3 = int.Parse(Console.ReadLine());

            if(num1>num2)
            {
                if (num1 > num3)
                {
                    Console.WriteLine("First number is greatest among three");
                }
                else
                {
                    Console.WriteLine("3rd number  is greatest among three");
                }
            }
            else if(num2>num3)
            {
                Console.WriteLine("2nd number is largest among three");
            }
            else
            {
                Console.WriteLine("3rd number is largest among three");
            }
        }
    }
}

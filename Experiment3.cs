﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReport
{
    public class Experiment3
    {
        public static void Exp3()
        {
            int n;
            int mm = 1, ctr = 0;
            Console.WriteLine("Please enter the number of elements to stored in the array:  ");
            n = int.Parse(Console.ReadLine());
            int[] arr1 = new int[n];
            int[] arr2 = new int[n];
            int[] arr3 = new int[n];
            Console.WriteLine("Enter the elements:  ");
            for(int i=0;i<n;i++)
            {
                arr1[i] = int.Parse(Console.ReadLine());    //something is wrong in this program
            }

            for(int i=0;i<n;i++)
            {
                arr2[i] = arr1[i];
                arr3[i] = 0;
            }

            for(int i=0;i<n;i++)
            {
                for(int j=0;j<n;j++)
                {
                    if(arr1[i]==arr2[j])
                    {
                        arr3[j] = mm;
                        mm++;
                    }
                }
                mm = 1;
            }

            for (int i = 0; i < n; i++)
            {
                if (arr3[i] == 2)
                    ctr++;
            }

            Console.WriteLine("The number of duplicate elements are:    "+ctr);

        }
    }
}
